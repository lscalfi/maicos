Authors list
############

.. include:: ../../../AUTHORS.rst

.. toctree::
   :maxdepth: 4
   :numbered:
   :hidden:
